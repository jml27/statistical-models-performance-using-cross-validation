
#-------------------------------------
#Binsmooth 
#-------------------------------------
#The function binsmoothREG performs a binsmooth regression with a user defined binlength
#Input Arguments:
#       binlength - amount of x values per bin (takes automatically the lenght of y from the dataset)

#### Note: the plots might not display correctly, since each model requires
########## a few manuel twiking in the errorPlot function

binsmoothREG <- function(binlength=length(dat$mpg), knotsdef=NULL)
{
  set.seed(123456)
  error_tot <- vector()
  rss_tot <- vector()
  min_vec <- vector()
  rss_vec <- vector()
  errT_tot <- vector()
  tError_vec <- vector()
  index <- vector()
  trErrors <- matrix(nrow = 4,ncol = 294)
  teErrors <- matrix(nrow = 4,ncol = 294)
  rss_mat <- matrix(nrow = 4,ncol = 294)
  binlen <- 314
  count <- 0
  w <- 0
  
  for(cov in 2:5){
    count <- count + 1
    for(l in 1:binlength){
      nbin <- 0
      for(f in 1:5){
        trainData <- cvData(dat,folds,f,0)
        testData <- cvData(dat,folds,f,1)
        
        #scale data to [0,1]
        x <- trainData[,cov]
        x<-x-min(x)
        x<-x/max(x)
        
        nbin <- length(x)
        mod_coef <- matrix(nrow = nbin)
        
        xtest <- testData[,cov]
        xtest<-xtest-min(xtest)
        xtest<-xtest/max(xtest)
        
        #Sort x values in ascending order
        y <- trainData$mpg
        ytest <- testData$mpg
        y <- y[order(x)]
        ytest <- ytest[order(xtest)]
        x <- sort(x)
        xtest <- sort(xtest)
        n <- length(x)
        
        bins <- ceiling(length(x) / l)
        
        #Create Design Matrix without intercept
        DM <- matrix(1,length(x),bins)
        DMT <- matrix(1,length(xtest),bins)
        binlengthTest <- (length(xtest)/bins)
        #Set all elements not corresponding to region j equal 0
        for(i in 1:bins)
        {
          if(i==1) { xstart = 1 }
          if(i>1) { xstart = (i-1)*l+1 }
          xend = min(xstart + l-1, length(x))
          binelements <- xstart:xend
          elements <- 1:length(x)
          elements[binelements] <- 0
          DM[elements,i] <- 0
        }
        
        for(j in 1:bins)
        {
          if(j==1) { xstart1 = 1 }
          if(j>1) { xstart1 = (j-1)*binlengthTest+1 }
          xend1 = min(xstart1 + binlengthTest-1, length(xtest))
          binelements1 <- xstart1:xend1
          elements1 <- 1:length(xtest)
          elements1[binelements1] <- 0
          DMT[elements1,j] <- 0
        }
        
        #Perform Linear Regreesion
        reg <- lm(y~0+DM)
        
        # prediction
        mod_coef <- (coef(reg))
        yhat <- DMT %*% mod_coef
        error <- sqrt(sum((ytest - yhat)^2)/length(ytest))
        rss <- sum((ytest - yhat)^2)
        rss_tot <- c(rss_tot,rss)
        error_tot <- c(error_tot,error)
        errT <- sqrt(sum((residuals(reg))^2)/length(y))
        errT_tot <- c(errT_tot,errT)
      }
      mu_error <- mean(error_tot)
      mu_trError <- mean(errT_tot)
      mu_rss <- mean(rss_tot)
      error_tot <- vector()
      errT_tot <- vector()
      rss_tot <- vector()
      min_vec <- c(min_vec,mu_error)
      tError_vec <- c(tError_vec,mu_trError)
      rss_vec <- c(rss_vec,mu_rss)
      
      if(l <= nbin){
        if(l >= 20){
          if(length(index) <= 293){
            index <- c(index,l)
          }
          
          w <- w+1
          if(w <= 294){
            teErrors[count,w] <- mu_error
            trErrors[count,w] <- mu_trError
            rss_mat[count,w] <- mu_rss
          }
        }
      }else{
        break
      }
    }
    
    min_error <- min(min_vec)
    min_rss <- min(rss_vec)
    pos <- match(min_error,min_vec)
    min_vec <- vector()
    tError_vec <- vector()
    rss_vec <- vector()
    
    w <- 0
    cat("------------------------------","\n")
    cat("covariate name: ", names(dat[cov]), "\n")
    cat("------------------------------","\n")
    cat("optimal binlength: ", pos, "\n")
    cat("optimal number of bins: ", (ceiling(314/pos)), "\n")
    cat("min mse error: ", min_error, "\n")
    cat("rss: ", min_rss, "\n")
  }
  
    binlen <- length(teErrors[1,])
    plotFun(teErrors,trErrors,0,binlen,"bsm",index)
  
}
