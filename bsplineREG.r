#-------------------------------------
#Bspline regression 
#The following code is heavily based on code of
#Dr. Samiran Sinha (Department of Statistics, Texas A&M)
#http://www.stat.tamu.edu/~sinha/research/note1.pdf 
#And Jeffrey S. Racine: A PRIMER ON REGRESSION SPLINES
#-------------------------------------
#The function bsplinereg performs a bspline regression with user defined knots
# Returns training and prediction errors, as well as associated RSS values
#Input Arguments:
#Input Arguments bsplinereg(...):
#       trainData - dataframe of training data
#       testData - dataframe of test data
#       knots - number of knots in [0,1]
#       degree - degree of the polynomial
#       pos - position of the attribute in the data frame
#       ouptut - 1: delivers some output, 0: no output
#       opt   - 1: returns adj R-squared, 0: returns nothing
#       ploto - 1: Create new plot, 0: no new plot

#### Note: the plots might not display correctly, since each model requires
########## a few manuel twiking in the errorPlot function

#------------------------------------------------------------
# Run the crossVal function for this file
#------------------------------------------------------------

#Calculate basis (rekursiv)
basis <- function(x, degree, i, knots) 
{ 
  
  if(degree == 0)
  { B <- ifelse((x >= knots[i]) & (x < knots[i+1]), 1, 0) 
  } else { 
    if((knots[degree+i] - knots[i]) == 0) 
    { alpha1 <- 0 
    } else { 
      alpha1 <- (x - knots[i])/(knots[degree+i] - knots[i]) } 
    
    if((knots[i+degree+1] - knots[i+1]) == 0) 
    { alpha2 <- 0 
    } else { alpha2 <- (knots[i+degree+1] - x)/(knots[i+degree+1] - knots[i+1]) } 
    B <- alpha1*basis(x, (degree-1), i, knots) + alpha2*basis(x, (degree-1), (i+1), knots) 
  } 
  
  return(B) 
}

#Create bspline Desin Matrix
bspline <- function(x, degree, knotpos) 
{ 
  #Number of basis
  K <- length(knotpos) + degree + 1 
  #Number of observations
  n <- length(x)
  #Set Boundary knots
  Boundary.knots = c(0,1)
  #create new vector with knot positons 
  knotpos <- c(rep(Boundary.knots[1], (degree+1)), knotpos, rep(Boundary.knots[2], (degree+1))) 
  
  
  #Create design matrix
  DM <- matrix(0,n,K) 
  for(j in 1:K) DM[,j] <- basis(x, degree, j, knotpos) 
  if(any(x == Boundary.knots[2])) DM[x == Boundary.knots[2], K] <- 1 
  #Return DM  
  return(DM) 
}


bsplinereg <- function(trainData, testData, knots,degree,pos, output=1, ploto=1, opt=0)
{
  
  x <- trainData[,pos]
  y <- trainData$mpg
  
  xtest <- testData[,pos]
  ytest <- testData$mpg
  
  knotsdef <- NULL
  
  #Scale data to [0,1]
  x<-x-min(x)
  x<-x/max(x)
  
  xtest<-xtest-min(xtest)
  xtest<-xtest/max(xtest)
  #Sort x values in ascending order
  y <- y[order(x)]
  x <- sort(x)
  
  ytest <- ytest[order(xtest)]
  xtest <- sort(xtest)
  
  n <- length(x)
  
  #Calculate knot postions
  if(knots == 0) knotpos <- NULL
  if(knots != 0) knotpos <- 1:knots / (knots+1)
  if(length(knotsdef)>0) knotpos <- knotsdef
  #Create Design Matrix 
  DM <- bspline(x, degree, knotpos)
  
  # create design matrix for test
  DMT <- bspline(xtest, degree, knotpos)
  
  #Perform penalized regression
  reg <- lm(y ~ 0 + DM)
  #print(summary(reg))
  
  mod_coef <- as.matrix(coef(reg))
  
  yHat <- DMT %*% mod_coef
  
  error <- sqrt(sum((ytest - yHat)^2)/length(ytest))
  train_error <- sqrt(sum((residuals(reg))^2)/length(trainData$mpg))
  rssTest <- sum((ytest - yHat)^2)
  #Calculate goodness of fit measures
  q <- length(knotpos) + degree + 1
  #Residual sum of squares
  rss <-  sum(sapply(residuals(reg), function(x) { x^2 }))
  #Coefficient of determination: R^2
  R2 <- 1 - (rss/ (t(y)%*%y-(mean(y)**2*n)))
  #Adjusted Coefficient of determination: R^2
  R2adj <- 1 - ( (n-1)/(n-q) ) * (1-R2)   
  #AIC
  aic <- AIC(reg)
  #cat("RMSE: ",error,"\n")
  return(c(error,train_error,rssTest))
}

# Performs cross validation
crossVal <- function(knots,deg){
  set.seed(123456)
  tErr_tot <- vector()
  tr_vec <- vector()
  rssT <- vector()
  avg_err <- 0
  avg_err2 <- 0
  avg_rss <- 0
  trErrors <- matrix(nrow = 4,ncol = knots)
  tErrors <- matrix(nrow = 4,ncol = knots)
  rss_mat <- matrix(nrow = 4,ncol = knots)
  count <- 0
  for(i in 1:knots){
    cat("---------------","\n")
    cat("Number of knots:", i,"\n")
    cat("---------------","\n")
    for(cov in 2:5){
      count <- count + 1
      varName <- names(dat)[cov]
      for(f in 1:length(folds)){
        trDat <- cvData(dat,folds,f,0)
        teDat <- cvData(dat,folds,f,1)
        errors <- bsplinereg(trDat,teDat,i,deg,cov)
        tErr_tot <- c(tErr_tot,errors[1])
        tr_vec <- c(tr_vec,errors[2])
        rssT <- c(rssT,errors[3])
      }
      avg_err <- mean(tErr_tot)
      avg_err2 <- mean(tr_vec)
      avg_rss <- mean(rssT)
      tErr_tot <- vector()
      tr_vec <- vector()
      rssT <- vector()
      cat(varName,"RMSE: ", avg_err, "\n")
      
      trErrors[count,i] <- avg_err2 # training error matrix
      tErrors[count,i] <- avg_err # prediction error matrix
      rss_mat[count,i] <- avg_rss # rss matrix
    }
    count <- 0
  }
  plotFun(tErrors,trErrors,knots,knots,"bs")
}
