
plotFun <- function(yAxisTeVal,yAxisTrVal,xRan=0,xlimUp,funType,index=NULL){
  par(mfrow=c(1,2))
  if(funType == "pl"){
    xlabel <- "Polynomial degrees"
    mainT <- "Polynomial "
    size <- 0.75
  }
  if(funType == "bs"){
    #par(mfrow=c(2,2))
    xlabel <- "Number of knots"
    mainT <- "B-spline "
    size <- 0.8
  }
  if(funType == "bsm"){
    xlabel <- "Bin Length"
    mainT <- "Binsmooth "
    size <- 0.7
  }
  
  for(p in 1:4){
    varName <- names(dat)[p+1]
    xAxis <- 1:xRan
    if(funType == "bsm"){
      xAxis <- index
    }
    yAxisTe <- yAxisTeVal[p,]
    yAxisTr <- yAxisTrVal[p,]
    yRanTe <- range(yAxisTe)
    yRanTr <- range(yAxisTr)
    
    plot(xAxis,yAxisTr,main = paste0(mainT,length(folds),"-fold CV:\n ",varName), type = "l",
         ylim = c(yRanTr[1],yRanTe[2]),xlim=c(1,xlimUp),ylab = "Prediction error (RMSE)",xlab = xlabel)
    lines(xAxis, yAxisTr, col = "darkblue", lwd = 2)
    par(new=T)
    plot(xAxis,yAxisTe,type = "l",ylim = c(yRanTr[1],yRanTe[2]),xlim=c(1,xlimUp),ylab = "Prediction error (RMSE)",xlab = xlabel)
    lines(xAxis, yAxisTe, col = "darkred",lwd = 2)
    
    
    points(xAxis[which.min(yAxisTe)], yAxisTe[which.min(yAxisTe)], col = "darkorange", pch = 16,cex = 1.5)
    abline(h= yAxisTe[which.min(yAxisTe)], col="purple",lty=2)
    abline(v= xAxis[which.min(yAxisTe)], col="purple",lty=2)
    yran <- yRanTe[2]
    if(funType == "pl"){
      xran <- 3
      extr <-paste("degree chosen:",round(xAxis[which.min(yAxisTe)],digits=3))
      if(p==1) {xran = 4}
    }
    
    if(funType == "bs"){
      xran <- 1
      if(p==1) {yran = 4.2}
      if(p==2) {yran = 4.35 
      xran = 3}
      if(p==3) {yran = 4.1}
      if(p==4) {yran = 7.6
      xran = 2}
      
      extr <-paste("knots chosen:",round(xAxis[which.min(yAxisTe)],digits=3))
    }
    
    if(funType == "bsm"){
      yran <- 11
      if(p==4) {yran = 7}
      xran <- 100
      extr <-paste("Bin length:",round(xAxis[which.min(yAxisTe)],digits=3),"\nBins:",ceiling(314/xAxis[which.min(yAxisTe)]))
    }
    
      legend(x=xran,y=yran, legend = c("Training error", "Test error",paste("Min pred RMSE:",round(yAxisTe[which.min(yAxisTe)],digits=3)),extr)
             ,col = c("darkblue","darkred","darkorange","purple"),lty = 1:4,cex = size,lwd = 3)
    }
}
